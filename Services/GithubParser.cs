﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using GitSync.Utils;
using Newtonsoft.Json.Linq;

namespace GitSync.Services
{
	public partial class GithubParser : ServiceParser
	{
		public override string Identifier => "Github";

		public override void Parse(Webhook webhook,HttpListenerRequest request,string jsonText,out bool hasCommits)
		{
			hasCommits = false;

			JObject json = JObject.Parse(jsonText) ?? throw new InvalidDataException("Invalid json.");

			if(webhook.secret!=null) {
				const string SignatureHeader = "X-Hub-Signature";

				var signature = request.Headers[SignatureHeader];

				if(string.IsNullOrWhiteSpace(signature)) {
					throw new InvalidDataException($"No '{SignatureHeader}' header provided in the request.");
				}

				var jsonData = Encoding.UTF8.GetBytes(jsonText);
				var secretData = Encoding.UTF8.GetBytes(webhook.secret);

				using var hmac = new HMACSHA1(secretData);

				var bs = hmac.ComputeHash(jsonData);
				var sha1 = BitConverter.ToString(bs).ToLower().Replace("-","");

				if(signature!=$"sha1={sha1}") {
					throw new InvalidDataException($"Invalid hash in {SignatureHeader} header.");
				}

				ConsoleUtils.WriteLineColored("Secret hash is valid.",ConsoleColor.Green);
			}

			hasCommits = json.ContainsKey("commits") && json["commits"] is JArray array && array.Count>0;
		}
	}
}
