﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;

namespace GitSync.Services
{
	public abstract class ServiceParser
	{
		private static Dictionary<string,ServiceParser> parsers;

		public abstract string Identifier { get; }

		public abstract void Parse(Webhook webhook,HttpListenerRequest request,string jsonText,out bool hasCommits);

		public static void Init()
		{
			parsers = new Dictionary<string,ServiceParser>(StringComparer.InvariantCultureIgnoreCase);

			foreach(var type in Assembly.GetExecutingAssembly().GetTypes().Where(t => !t.IsAbstract && typeof(ServiceParser).IsAssignableFrom(t))) {
				var instance = (ServiceParser)Activator.CreateInstance(type);

				parsers.Add(instance.Identifier,instance);
			}
		}
		public static ServiceParser GetInstance(string identifier) => parsers.TryGetValue(identifier,out var result) ? result : throw new ArgumentException($"Unknown service/parser: '{identifier}'.");
	}
}
