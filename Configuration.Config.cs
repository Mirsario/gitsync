﻿using Newtonsoft.Json;

namespace GitSync
{
	partial class Configuration
	{
		public class Config
		{
			[JsonRequired]
			public string gitPath = "git";

			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public string[] listenUrls = { "127.0.0.1:9001" };

			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public Webhook[] webhooks = new[] {
				new Webhook {
					relativeUrl = "/webhooks/github",
					serviceType = "Github",
					remoteLink = "https://github.com/Mirsario/MopBot.git",
					directory = "MopBot"
				}
			};
		}
	}
}
