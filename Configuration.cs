﻿using System;
using System.IO;
using GitSync.Utils;
using Newtonsoft.Json;

namespace GitSync
{
	public static partial class Configuration
	{
		public readonly static string ConfigPath = $"{nameof(GitSync)}.json";

		public static Config Current { get; private set; }

		public static void Init()
		{
			if(File.Exists(ConfigPath)) {
				try {
					Current = JsonConvert.DeserializeObject<Config>(File.ReadAllText(ConfigPath));
				}
				catch(Exception e) {
					ConsoleUtils.WriteLineColored($"Unable to read '{Path.GetFullPath(ConfigPath)}'.\r\n{e.GetType().Name}: {e.Message}",ConsoleColor.Red);
				}
			}

			if(Current==null) {
				Current = new Config();
			}

			File.WriteAllText(ConfigPath,JsonConvert.SerializeObject(Current,Formatting.Indented));

			if(!VerifyConfig()) {
				Console.WriteLine($"Please configure the program through the '{ConfigPath}' file.");
			}
		}

		private static bool VerifyConfig()
		{
			return true;
		}
	}
}
