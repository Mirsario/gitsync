﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using GitSync.Services;
using GitSync.Utils;

namespace GitSync
{
	public class Program
	{
		private static void Main()
		{
			Console.WriteLine("Initializing...");

			try {
				Configuration.Init();
				ServiceParser.Init();

				var listener = new Listener();

				listener.OnPostRequest += (request,data,response) => {
					var webhooks = Configuration.Current.webhooks;

					for(int i = 0;i<webhooks.Length;i++) {
						var webhook = webhooks[i];

						string requestUrl = UrlUtils.MakeRelative(request.Url.AbsoluteUri);
						string webhookUrl = UrlUtils.MakeRelative(webhook.relativeUrl);

						if(requestUrl!=webhookUrl) {
							continue;
						}

						Console.WriteLine($"Webhook {webhook.relativeUrl} request from {request.RemoteEndPoint}");

						var service = ServiceParser.GetInstance(webhook.serviceType);
						bool hasCommits;

						try {
							service.Parse(webhook,request,data,out hasCommits);
						}
						catch(Exception e) {
							ConsoleUtils.WriteLineColored(e.Message,ConsoleColor.Red);

							response.StatusCode = 400; //Bad Request.

							continue;
						}

						response.StatusCode = 202; //Accepted.

						if(hasCommits) {
							UpdateRepo(webhook);
						}

						ConsoleUtils.WriteLineColored("Webhook processed.",ConsoleColor.Green);
					}
				};

				listener.Listen(Configuration.Current.listenUrls);
			}
			catch(Exception e) {
				ConsoleUtils.WriteLineColored($"An {e.GetType().Name} has occured: {e.Message}\r\n{e.StackTrace}",ConsoleColor.Red);

				Thread.Sleep(5000); //ReadLine would block systemctl restarts.
			}
		}
		private static void UpdateRepo(Webhook webhook)
		{
			Console.WriteLine($"Updating repo {webhook.directory}...");

			string workingDirectory = Path.GetFullPath(webhook.directory);

			bool firstRun = !Directory.Exists(workingDirectory) || !Directory.Exists(Path.Combine(workingDirectory,".git"));

			if(firstRun) {
				Directory.CreateDirectory(workingDirectory);

				Git(workingDirectory,"init");
				Git(workingDirectory,$"remote add origin https://{webhook.remoteLink}.git");
			}

			Git(workingDirectory,"fetch origin");

			if(firstRun) {
				Git(workingDirectory,"checkout origin/master -ft");
			} else {
				Git(workingDirectory,"pull");
			}

			if(!string.IsNullOrWhiteSpace(webhook.command)) {
				ConsoleUtils.WriteLineColored("Running command...",ConsoleColor.White);

				RunCommand(webhook.command,workingDirectory);
			}
		}
		private static void RunCommand(string command,string workingDirectory)
		{
			var startInfo = new ProcessStartInfo {
				WorkingDirectory = workingDirectory,
				RedirectStandardOutput = true,
				UseShellExecute = false
			};

			if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
				startInfo.FileName = "cmd.exe";
				startInfo.Arguments = $"/C {command}";
			} else {
				startInfo.FileName = "/bin/bash";
				startInfo.Arguments = $@"-c ""{command.Replace(@"""",@"\""")}""";
			}

			Process.Start(startInfo).WaitForExit();
		}
		private static void Git(string workingDirectory,string arguments)
		{
			ConsoleUtils.WriteLineColored($"Running 'git {arguments}'...",ConsoleColor.White);

			Process.Start(new ProcessStartInfo {
				FileName = Configuration.Current.gitPath,
				WorkingDirectory = workingDirectory,
				Arguments = arguments,
				RedirectStandardOutput = true
			}).WaitForExit();
		}
	}
}
