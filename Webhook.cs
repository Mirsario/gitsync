﻿using Newtonsoft.Json;

namespace GitSync
{
	public class Webhook
	{
		public string command;
		public string secret;

		[JsonRequired]
		public string relativeUrl;

		[JsonRequired]
		public string remoteLink;

		[JsonRequired]
		public string directory;

		[JsonRequired]
		public string serviceType;
	}
}
