﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace GitSync
{
	public class Listener
	{
		public delegate void PostRequestEvent(HttpListenerRequest request,string textData,HttpListenerResponse response);

		public event PostRequestEvent OnPostRequest;

		public void Listen(string[] prefixes)
		{
			using HttpListener listener = new HttpListener();

			for(int i = 0;i<prefixes.Length;i++) {
				listener.Prefixes.Add($"http://{prefixes[i]}/");
			}

			listener.Start();

			Console.WriteLine($"Listening at: {string.Join(',',listener.Prefixes.Select(prefix => $"'{prefix}'"))}");

			while(true) {
				var context = listener.GetContext(); //Blocks the thread.
				var request = context.Request;
				var response = context.Response;

				using var streamReader = new StreamReader(request.InputStream,request.ContentEncoding);

				string text = streamReader.ReadToEnd();

				response.StatusCode = 404; //Not Found.

				OnPostRequest(request,text,response);

				//Respond
				byte[] buffer = Encoding.UTF8.GetBytes($"{response.StatusCode} - {response.StatusDescription}");

				response.ContentLength64 = buffer.Length;

				response.OutputStream.Write(buffer,0,buffer.Length);
			}
		}
	}
}
