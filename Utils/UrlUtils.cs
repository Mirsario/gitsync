﻿using System.Text.RegularExpressions;

namespace GitSync.Utils
{
	public static class UrlUtils
	{
		public static readonly Regex UrlToRelativeRegex = new Regex(@"^(?:(?:http|https):\/\/(?:[\w.:]+))?\/?(.+?)\/?$",RegexOptions.Compiled);

		public static string MakeRelative(string url) => UrlToRelativeRegex.Replace(url,"/$1");
	}
}
