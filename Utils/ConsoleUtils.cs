﻿using System;

namespace GitSync.Utils
{
	public static class ConsoleUtils
	{
		public static void WriteLineColored(object text,ConsoleColor color)
		{
			Console.ForegroundColor = color;

			Console.WriteLine(text);

			Console.ForegroundColor = ConsoleColor.Gray;
		}
	}
}
